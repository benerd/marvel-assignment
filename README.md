# STAR WARS - MARVEL (Mrinal Purohit) 

# prerequisites to be installed for build

1. node
2. yarn

# steps to build

Run the following in terminal/cmd in the order:

1. git clone git@bitbucket.org:benerd/marvel-assignment.git
2. cd marvel-assignment
3. yarn
4. yarn start
5. Navigate to localhost:3000/

# to run tests

Run the following in terminal/cmd:

1. yarn test
