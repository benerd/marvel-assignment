import {loadingPlanets} from './index';

//test suit for action dispatchers
describe('this suite tests action dispatchers', () => {
  
  //basic test for action dispatcher (without thunk)
  it('creates action dispatcher for loaders', () => {
    expect(loadingPlanets(true)).toEqual({type: 'LOADING_PLANETS', value: true})
  })

  //TODO: thunk based dispatchers testing using redux-testkit - left for this demo but can be implemented
  
});
