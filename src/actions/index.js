import axios from 'axios';
import {GET_PLANETS, LOADING_PLANETS, GET_SEARCHED_PLANETS} from '../constants/action-types';
import {api_base_url} from '../constants/config';

/* IMPLEMENTATION OF SELECTOR PATTERN OMMITTED FOR THIS DEMO - but should be implemented */
export const getPlanets = planets => {
    return (dispatch) => {
        axios
            .get(api_base_url + 'planets?page=' + planets.page)
            .then(resp => {
                dispatch({type: GET_PLANETS, planets: resp.data.results})
            });
    }
};

export const loadingPlanets = isLoading => {
    return {type: LOADING_PLANETS, value: isLoading}
};

export const searchPlanets = planets => {
    return (dispatch) => {
        axios
            .get(api_base_url + 'planets?page=' + planets.page + '&&search=' + planets.search)
            .then(resp => {
                dispatch({type: GET_SEARCHED_PLANETS, planets: resp.data.results})
            });
    }
};