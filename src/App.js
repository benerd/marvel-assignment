import React, {Component} from 'react';
import './App.css';
import Login from './components/login/login';
import Dashboard from './components/dashboard/dashboard';
import {
    Switch,
    Route
} from 'react-router-dom';

class App extends Component {
    render() {
        return (<Switch>
                    <Route exact path="/login" component={Login}/>
                    <Route path="/dashboard" component={Dashboard}/>
                    <Route component={Login}/>
                </Switch>
        );
    }
}

export default App;
