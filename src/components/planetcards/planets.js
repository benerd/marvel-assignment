import React, {Component} from 'react';
import './planet-cards.css';
import {connect} from 'react-redux';
const mapStateToProps = state => ({planets: state.planets})
class ConnectedPlanet extends Component {
    
    getPlanetNameFontSize() {
        
        //NOTE: due to high spread between the populations i.e around 1000 to 1000000000000, the variance is less.
        // Calculations for standard deviation taken from: http://www.mathsisfun.com/data/standard-deviation-calculator.html
        let population = this
            .props
            .planets
            .data
            .map(planet =>+ planet.population)
            .filter(population => !isNaN(population));

        //CALCULATING MEAN AND STANDARD DEVIATION
        let mean = population.reduce((a, b) => a + b) / population.length, //mean of all the values
        differences = population.map(planetPopulation => planetPopulation - mean), //Every Number minus the Mean
        differencesSquare = differences.map(diff => diff ** 2), //Square of each difference
        sumOfdifferencesSquare = differencesSquare.reduce((a, b) => a + b), //Add up the Squared Differences
        variance = sumOfdifferencesSquare / population.length, //Sum of squared differences / Count
        standardDeviation = Math.sqrt(variance), //square root of variance
        scaledVals = population.map(planetPopulation => {
            return (planetPopulation - mean) / standardDeviation;
        }),
         minScale = Math.min(...scaledVals),
         maxScale = Math.max(...scaledVals),
         scaleLowerBound = 10,
         scaleHigherBound = 40;
        
        if (isNaN(+ this.props.data.population)) { //value of population is 'unknown' or non-number
            return scaleLowerBound;
        } else {
            let scaledx = (+ this.props.data.population - mean) / standardDeviation, //mapping new meaned scale to desired one
            fontSize = scaleLowerBound + Math.abs(scaledx) / (maxScale - minScale) * 30;
            return Math.round(fontSize,2);
        }

    }
    render() {
        return <div className="col-md-4 mb-5 warrior-card">
            <h2
                style={{
                fontSize: this.getPlanetNameFontSize() + 'px'
            }}>{this.props.data.name}
                ({this.getPlanetNameFontSize() + 'px'})</h2>
            <p>
                <b>Gravity:
                </b>{this.props.data.gravity}<br/>
                <b>Climate:
                </b>{this.props.data.climate}
                <br/>
                <b>Rotation Period:
                </b>{this.props.data.rotation_period}<br/>
                <b>Population:
                </b>{this.props.data.population}<br/>
            </p>
            <p>
                <button className="btn btn-secondary btn-success">View details »</button>
            </p>
        </div>
    }
}
let Planet = connect(mapStateToProps, null)(ConnectedPlanet);
export
default Planet;