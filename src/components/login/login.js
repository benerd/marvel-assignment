import React, {Component} from 'react';
import axios from 'axios';
import {withRouter} from 'react-router'
import swal from 'sweetalert';
import './login.css';

class Login extends Component {
    loginUser(e) {
        e.preventDefault();
        let username = e.target.username.value,
            password = e.target.password.value;
        axios
            .get('https://swapi.co/api/people?search=' + username)
            .then(warriors => {
                if (warriors.data.count) {
                    if (warriors.data.results[0].birth_year === password) {
                        localStorage.setItem('loggedInAs', JSON.stringify(warriors.data.results[0]));
                        this
                            .props
                            .history
                            .push('/dashboard');
                    } else {
                        this.showError()
                    }
                }
                else
                {
                    this.showError();
                }
            })
            .catch(err => console.log(err))

    }
    showError() {
        swal({title: "Error", text: "No Warrior Available with this username/password", icon: "warning"})
    }
    render() {
        return <div className="login-page justify-content-center align-self-center">
            <div className="form">
                <form
                    className="login-form"
                    onSubmit={this
                    .loginUser
                    .bind(this)}>
                    <h4>Welcome back, hero!</h4>
                    <input type="text" name="username" placeholder="username"/>
                    <input type="password" name="password" placeholder="password"/>
                    <button>login</button>
                </form>
            </div>
        </div>
    }
}

export default withRouter(Login);