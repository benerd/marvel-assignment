import React, {Component} from 'react';
import {connect} from 'react-redux';
import Navbar from '../shared/navbar/navbar';
import Search from '../search/search';
import {getPlanets} from '../../actions';
import Warrior from '../planetcards/planets';
import {withRouter} from 'react-router';
import './dashboard.css';

const mapDispatchToProps = dispatch => {
  return {
    getPlanets: warriorData => dispatch(getPlanets(warriorData))
  };
}
const mapStateToProps = state => {
  return {planets: state.planets.data, loadingPlanets: state.planets.loading}
}

class ConnectedDashboard extends Component {

  componentDidMount() {

    //check if user is logged in
    this.checkIfLoggedIn();

    //loading first page of planets
    let currentPage = 1;
    this
      .props
      .getPlanets({page: currentPage});

    //lazy (infinite) load on scroll
    window.onscroll = (ev) => {
      if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
        this
          .props
          .getPlanets({
            page: ++currentPage
          });
      }
    };
  }
  checkIfLoggedIn() {
    // check if user is loggedin no serverside tokenization available - simple local
    // storage checking of current user for demo purposes
    if (!JSON.parse(localStorage.getItem('loggedInAs'))) {
      this
        .props
        .history
        .push('/login')
    }
  }
  getPlanetCards() {
    if (this.props.planets.length) 
      return this.props.planets.map((warrior, index) => <Warrior data={warrior} key={index}/>)
    else 
      return <div className="text-center col-md-12 mt-5 mb-5">Oops! Seems no planet with this
        search criteria wish to participate in the war! Try some other planet!</div>
  }

  getLoader() {
    return <div className="text-center col-md-12 mt-5 mb-5">Planets are spinning up and should be stable soon...</div>
  }
  render() {
    return <div>
      <Navbar/>
      <main role="main">
        <Search/>
        <div className="container">
          <div className="row">
            {this.props.loadingPlanets
              ? this.getLoader()
              : this.getPlanetCards()}
          </div>
          <hr/>
        </div>
      </main>
      <footer className="container">
        <p>© The Starluous War Company 2017-2018</p>
      </footer>
    </div>
  }
}

const Dashboard = withRouter(connect(mapStateToProps, mapDispatchToProps)(ConnectedDashboard));
export default Dashboard;