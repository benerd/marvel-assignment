import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {withRouter} from 'react-router';
class NavBar extends Component {
  logoutUser(e){
    localStorage.setItem('loggedInAs',null);
    this.props.history.push('/login')
  }
  render(){
    return   (<nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <Link className="navbar-brand" to="dashboard">Star Wars Ahoy!</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse">
        <ul className="navbar-nav mr-auto">
          
        </ul>
        <form className="form-inline mt-2 mt-md-0">
          <button className="btn btn-outline-success my-2 my-sm-0" onClick={this.logoutUser.bind(this)}>Logout</button>
        </form>
         
        </div>
      </nav>);
  }
}
export default withRouter(NavBar);