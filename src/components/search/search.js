import React, {Component} from 'react';
import {connect} from 'react-redux';

import {DebounceInput} from 'react-debounce-input';
import {searchPlanets,loadingPlanets} from "../../actions";


const mapDispatchToProps = dispatch => {
    return {
        searchPlanets: actionPayload => dispatch(searchPlanets(actionPayload)),
        loadingPlanets: isLoading => dispatch(loadingPlanets(isLoading))
    };
}

class ConnectedSearch extends Component {
    constructor(props){
        super(props);
        this.currentPage = 1;
    }
    search(e){
        let searchTerm = e.target.value;
        let action = {
            page:this.currentPage,
            search:searchTerm
        };

        this.props.loadingPlanets(true);
        this.props.searchPlanets(action);
    }

    render() {
        return <div className="jumbotron">
            <div className="container">
                <h3 className="w-100 pt-3 text-center">Let the war begin! Search planets...</h3>
                <form className="form-inline w-100 mt-2">
                    <DebounceInput
                        minLength={2}
                        debounceTimeout={300} onChange={this.search.bind(this)}
                        className="form-control mr-sm-2 w-100"
                        type="text"
                        placeholder="Search"
                        aria-label="Search"/>

                </form>
            </div>
        </div>
    }
}
let Search = connect(null,mapDispatchToProps)(ConnectedSearch);
export default Search;
