const initialState = {
    data: [],
    loading: true
};
let Planets = (state = initialState, payload) => {
    switch (payload.type) {
        case "GET_PLANETS":
            return {data: [...state.data, ...payload.planets], loading: false}
        case "GET_SEARCHED_PLANETS":
            return {data: payload.planets, loading: false}
        case "LOADING_PLANETS":
            return {data:[...state.data],loading:payload.value}
        default:
            return state;
    }
}
export default Planets;