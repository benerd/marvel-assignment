import {combineReducers} from 'redux';
import Planets from './planets';

const rootReducer =combineReducers({
  planets: Planets
});

export default rootReducer;