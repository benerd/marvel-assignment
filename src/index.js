import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose} from "redux";

import thunk from 'redux-thunk';

import rootReducer from "./reducers";
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ||
// compose; const enhancer = composeEnhancers(   applyMiddleware(thunk),
// window.devToolsExtension ? window.devToolsExtension() : f => f );

let store = createStore(rootReducer, compose(applyMiddleware(thunk), window.devToolsExtension
    ? window.devToolsExtension()
    : f => f));
ReactDOM.render(
    <Provider store={store}>
    <BrowserRouter>
        <App/>
    </BrowserRouter>
</Provider>, document.getElementById('root'));
registerServiceWorker();
